var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        txn_ref: '',
        formShown: true,
        detailShown: false
    },
    created: {
        
    },
    methods: {
        async processForm() {
            this.formShown = false
            this.detailShown = true
            var config = {
                method: 'post',
                url: 'http://chbck.test/api/v1/vervesearch',
                data : {'ref': this.txn_ref}
              };
            let res = await axios(config);
            console.log(res);
           
        }
    }
 })