$(document).ready(function () {
  //  Controll the input
  $('#provider').change(function () {
    var provider = $(this).find(':selected').val()
    if (provider == 'mpgs') {
      $('#approval').prop('disabled', false)
      $('#date').prop('disabled', false)
      $('#txn').prop('disabled', true)
      $('#amount').prop('disabled', true)
    } else {
      $('#txn').prop('disabled', false)
      $('#amount').prop('disabled', false)
      $('#approval').prop('disabled', true)
      $('#date').prop('disabled', true)
    }
  })
  // Control the query now

  $('form').validate({
    submitHandler: function (form) {
      $('.main').hide()
      $('.loading').show()
      var formValues = $(form).serialize()
      axios.post('api/', formValues)
        .then((response) => {
          $('.loading').hide()
          // console.log(response)
          if (Object.keys(response.data).length > 0) {
            $('.result').show()
            $('#txn_ref').html(response.data.transaction_ref)
            $('#customer').html(response.data.customer_mobile_no)
            $('#status').html(response.data.response_status)
            $('#amount_result').html(response.data.amount)
            $('#date_now').html(response.data.updated_at)
            if (response.data.error_message != 'NULL') { $('#error').html(response.data.error_message) }
          } else {
            $('#error').html('Transaction not found.. Please contact the admin')
          }

        // $('#txn_ref').html(response.data.transaction_ref)
        }, (error) => {
          $('.loading').hide()
          $('.error_message').show()
          $('#error').html(error)
        })
      // alert('Form submitted....  ' + formValues)
    },
    rules: {
      provider: 'required',
      approval: 'required',
      date: 'required',
      txn: 'required'
    },
    errorElement: 'em',
    errorPlacement: function (error, element) {
      // Add the `invalid-feedback` class to the error element
      error.addClass('invalid-feedback')

      if (element.prop('type') === 'checkbox') {
        error.insertAfter(element.next('label'))
      } else {
        error.insertAfter(element)
      }
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid').removeClass('is-valid')
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).addClass('is-valid').removeClass('is-invalid')
    }
  })

  // $('form').submit(function (e) {
  //   e.preventDefault()
  //   $('.main').hide()
  //   $('.loading').show()
  //   var formValues = $(this).serialize()
  //   axios.post('api/', formValues)
  //     .then((response) => {
  //       $('.loading').hide()

  //       if (Object.keys(response.data).length > 0) {
  //         $('.result').show()
  //         $('#txn_ref').html(response.data.transaction_ref)
  //         $('#customer').html(response.data.customer_mobile_no)
  //         $('#status').html(response.data.response_status)
  //         $('#amount_result').html(response.data.amount)
  //         $('#date_now').html(response.data.updated_at)
  //         if (response.data.error_message != NULL) { $('#error').html(response.data.error_message) }
  //       } else {
  //         $('#error').html('Transaction not found.. Please contact the admin')
  //       }

  //       // $('#txn_ref').html(response.data.transaction_ref)
  //     }, (error) => {
  //       $('.loading').hide()
  //       $('.error_message').show()
  //       $('#error').html(error)
  //     })
  // })

  $('#requery').click(function () {
    location.reload()
  })
})
