# Payment Gateway Chargeback

This application developed using to query chargeback on the MTN payment gateway.

## Requirements

- PHP 7 or later version
- Ngnix or Apache
- Ngnix or Apache PostgresSQL driver  

## Installation

Clone the application from repository [bitbucket](https://codyphree@bitbucket.org/codyphree/chbk.git) to install chbck.

```bash
git clone https://codyphree@bitbucket.org/codyphree/chbk.git
```
## Usuage
To change database configuration navigate to  app/Config/Database.php
```php
public $default = [
		'DSN'      => 'POSTGRESQL DSN',
		'hostname' => 'HOSTNAME',
		'username' => 'USERNAME',
		'password' => 'PASSWORD',
		'database' => 'DATABASE',
		'DBDriver' => 'Postgre',
		'DBPrefix' => '',
		'pConnect' => false,
		'DBDebug'  => (ENVIRONMENT !== 'production'),
		'cacheOn'  => false,
		'cacheDir' => '',
		'charset'  => 'utf8',
		'DBCollat' => 'utf8_general_ci',
		'swapPre'  => '',
		'encrypt'  => false,
		'compress' => false,
		'strictOn' => false,
		'failover' => [],
		'port'     => 5432,
	];
```
Alternatively you can create .env on the root folder

```env
database.default.hostname = HOSTNAME
database.default.port = 5432
database.default.database = DATABASE
database.default.username = USERNAME
database.default.password = PASSWORD
database.default.DBDriver = postgre
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)