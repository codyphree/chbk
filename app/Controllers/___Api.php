<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Api extends ResourceController
{
    //protected $modelName = 'App\Models\Photos';
    protected $format = 'json';

    public function __construct()
    {
        $db = \Config\Database::connect();
    }

    public function index()
    {
         $request = \Config\Services::request();
         if($request->getVar('provider') == 'mpgs')
         {
            return $this->respond('Masterpass'); 
         }
         return $this->respond('Interswitch'); 
    }

    public function db_search()
    {
        $request = \Config\Services::request();
        
        if(!$request->getVar('ref'))
        {   $error = [
                'error_code' => 500,
                'message' => 'Error occured'
            ];
            return $this->respond($error);
        }
        $txn_id = trim($request->getVar('ref'));
        $transaction = $this->get_transaction_details($txn_id);
        $response = [
            'status' => 1,
            'message' => 'Transaction found',
            'transaction_details' => $transaction
        ];
        return $this->respond($response); 
    }
    public function migs()
    {
        $request = \Config\Services::request();
        
        if(!$request->getVar('ref'))
        {   $error = [
                'error_code' => 500,
                'message' => 'Error occured'
            ];
            return $this->respond($error);
        }

        $txn_id = trim($request->getVar('ref'));
        
        $migs = $this->queryMigs($txn_id);
        $response = [
            'status' => 1,
            'message' => 'Transaction found',
            'transaction_details' => $migs
        ];
        return $this->respond($response);
    }

    public function interswitch()
    {
        $request = \Config\Services::request();
      
        if(!$request->getVar('ref'))
        {   $error = [
                'error_code' => 500,
                'message' => 'Error occured'
            ];
            return $this->respond($error);
        }

        $txn_id = trim($request->getVar('ref'));
        //$amount = $this->get_transaction_details($txn_id)->txn_amount;
       $amount = trim($request->getVar('amount'));;
        $verve = $this->queryInterswitch($txn_id, $amount);
        $response = [
            'status' => 1,
            'message' => 'Transaction found',
            'transaction_details' => $verve
        ];
        return $this->respond($response); 
        
    }

    private function get_transaction_details($txn_id)
    {
        $db = \Config\Database::connect();
        $row = $db->query('select txn_id, txn_status,txn_amount,
         payment_option, txn_product_id, product_name, fullfilment from mtnepg_payment_transaction
         WHERE txn_id ="'.$txn_id.'"');
        return $row->getRow();
    }

    private function get_product_details($product_id, $payment_option)
    {
        $db = \Config\Database::connect();
        
        if($payment_option == 'migs')
        {
            $row = $db->query('select * from 
                mtnepg_portal_products_configurations as m
                join mtnepg_mig_configuration as t on
                m.mig_config_id = t.mig_config_id
                WHERE m.product_code ="'.$product_id.'"');
        } else {
            $row = $db->query('select * from 
                mtnepg_portal_products_configurations as m
                join mtnepg_interswitch_configuration as t on
                m.interswitch_config_id = t.interswitch_config_id
                WHERE m.product_code ="'.$product_id.'"');
        }
        
        return $row->getRow();
    }

    public function queryMigs($trans_id)
    {
        $vpcUrl = 'https://migs.mastercard.com.au/vpcdps';
        $product_code = substr($trans_id, 0, 4);
        $product_details = $this->get_product_details($product_code, 'migs');
        //Migs Parameter being set
        $vpcOptions = array();
        $vpcOptions['vpc_Version'] = '1'; // Default value
        $vpcOptions['vpc_Command'] = 'queryDR'; // Default value
        $vpcOptions['vpc_AccessCode'] = $product_details->AccessCode; //94CB6B00
        $vpcOptions['vpc_Merchant'] = $product_details->Merchant; //GTB722772B22
        $vpcOptions['vpc_MerchTxnRef'] = $trans_id; // vpc_OrderInfo
        $vpcOptions['vpc_OrderInfo'] = $trans_id; // vpc_OrderInfo 
        $vpcOptions['vpc_User'] = $product_details->operation_id; // NonGSMQuery operation_id
        $vpcOptions['vpc_Password'] = $product_details->password; // Good*1Things    GoodFor*1Us
        
        ob_start();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $vpcUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($vpcOptions));
        curl_exec($ch);
        $migsResponse = ob_get_contents();
        ob_end_clean();
        curl_close($ch);
        parse_str($migsResponse, $remoteResult);
        
        return $remoteResult;
       
    }

   public function queryInterswitch($trans_id, $transactionAmount) {
    $product_code = substr($trans_id, 0, 4);
    $product = $this->get_product_details($product_code, 'verve');
    //return $product;
    $productid = $product->product_id;
    $amount = $transactionAmount * 100;
    // $secret = "ogQbgSd3rQX5mcRnpD3HbJbJGj58bXbJgDjfT5ancyZIBFbwxYAPSI7BKg0RE6DWtlkRuC4xmh9rIAkxvtmN73j2QGpiJamTucwTT7fqawvTDSLUXgjGgOaN3Yj3T6XC"; // mac_key
    $secret = $product->mackey;
    $endpoint = "https://webpay.interswitchng.com/collections/api/v1/gettransaction.json?";	

    $params = array(
            "productid"=>$productid,
            "transactionreference"=>$trans_id,
            "amount"=>$amount
            ); 
        
    $query = http_build_query($params) . "\n";


     $url = $endpoint . urlencode($query); 	
    
     
        $hash_string = $productid.$trans_id.$secret;
        $hash = hash('sha512', $hash_string);	
        $hash = strtoupper($hash);
        
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
    
        $headers = array("UserAgent: $user_agent","Keep-Alive: 300","Connection: keep-alive","Hash: $hash");

        $ch = curl_init($url);

         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
        curl_setopt($ch, CURLOPT_POST, false );	
    
        $output = curl_exec($ch);
	
	     if (!curl_error($ch))
	     {
	     	return $response = json_decode($output, true);

         }
         
         return false;
    }
    
}
