<?php namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;

class Api extends ResourceController
{
    //protected $modelName = 'App\Models\Photos';
    protected $format = 'json';

    public function __construct()
    {
        $db = \Config\Database::connect();
    }

    public function index()
    {
         $date = date('Y-m-d', strtotime(' -1 day'));
         $db = \Config\Database::connect();
         $request = \Config\Services::request();
         header('Content-Type: application/json');
         $response = [];
         if($request->getVar('provider') == 'mpgs')
        {
           $sql = "SELECT transaction_log.transaction_ref,transaction_log.customer_mobile_no, transaction_log.response_status,transaction_log.error_message, transaction_log.transaction_final_amount/100 as Amount,transaction_log.updated_at FROM api_log JOIN transaction_log ON api_log.transaction_ref = transaction_log.transaction_ref WHERE CAST(api_log.response_body AS text) LIKE '%".$request->getVar('approval')."%' AND CAST(api_log.updated_at AS text) LIKE '%".$request->getVar('date')."%'";
            $response = $db->query($sql)->getRow(); 
       
        } else {

          $sql = "SELECT transaction_ref,customer_mobile_no, response_status,transaction_log.error_message, transaction_final_amount/100 as Amount,updated_at FROM transaction_log  WHERE transaction_ref = '".$request->getVar('txn')."'";
           $response = $db->query($sql)->getRow(); 
        }
        
         return $this->respond($response); 

    }


    public function getExample($provider, $approval = NULL, $date = NULL, $txn = NULL)
    {

        if($request->getVar('provider') == 'mpgs')
        {
           $sql = "SELECT transaction_log.transaction_ref,transaction_log.customer_mobile_no, transaction_log.response_status, transaction_log.transaction_final_amount/100 as Amount,transaction_log.updated_at FROM api_log JOIN transaction_log ON api_log.transaction_ref = transaction_log.transaction_ref WHERE CAST(api_log.response_body AS text) LIKE '%".$request->getVar('approval')."%' AND CAST(api_log.updated_at AS text) LIKE '%".$request->getVar('date')."%'";
            $response = $db->query($sql)->getRow(); 
       
        } else {

          $sql = "SELECT transaction_ref,customer_mobile_no, response_status, transaction_final_amount/100 as Amount,updated_at FROM transaction_log  WHERE transaction_ref like '".$request->getVar('txn')."%'";
           $response = $db->query($sql)->getRow(); 
        }
    }

}