<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>MTN Charge back</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
<style>
@import url('https://cdn.onepipe.io/assets/fonts/MTNBrighterSans/MTNBrighterSans.css');


html, body, #app {
    font-family: 'MTNBrighterSans-Regular' !important;
}

body {
  background-color: #3a3a3a !important;
    color: #000 !important;
}
.login-form {
    width: 340px;
    margin: 50px auto;
  	font-size: 15px;
}
.login-form .form{
    margin-bottom: 15px;
    background: #ffffff;
    box-shadow: 0px 2px 2px #fff;
    padding: 30px;
}
.login-form h2 {
    margin: 0 0 15px;
}
.form-control, .btn {
    min-height: 38px;
    border-radius: 2px;
    
}
.btn {        
    font-size: 15px;
    font-weight: bold;
     background-color: #ffca30 !important;
    border-color: #ffca30 !important;
    color: #000000 !important;
}

</style>
</head>
<body>
<div class="login-form">
  <!-- action="api" -->
    <form  method="post" id="form" class="form main">
        <h2 class="text-center">Chargeback Portal</h2>
        <div class="form-group">
         <select class="form-control" id="provider" name='provider'>
           <option value="">Select Payment Option</option>
           <option value='mpgs'>Mastercard/Visa</option>
           <option value='ipg'>Verve</option>
         </select>
        </div>
        <div class="form-group">
            <input type="text" class="form-control"  placeholder="Enter Approval Code" required="required" disabled="disabled" id='approval' name='approval'>
        </div>
      <div class="form-group">
            <input type="date" class="form-control"  placeholder="select transaction date" required="required" disabled="disabled" id='date' name='date'>
        </div>
        <div class="form-group">
              <input type="text" class="form-control"  placeholder="Enter transaction ref" disabled="disabled" required="required" id="txn"  name='txn'>
          </div>
        <!-- <div class="form-group">
              <input type="text" class="form-control"  placeholder="Enter transaction amount"disabled="disabled" required="required" id="amount" name='amount'>
          </div> -->
        <div class="form-group">
            <button type="submit" id="query" class="btn btn-primary btn-block">Query Now</button>
        </div>
    </form>
     <div class="loading text-center form" style="display: none;">
          <img src="loading.gif" alt="loading">
        </div>
    <div class="error_message text-center form" style="display: none;">
          <h1 id="errors"></h1>
        </div>
    <div class="form result" style="display: none; width: 440px !important">
      <h2 class="text-center">Result</h2>
      <table class="table table-hover table-sm table-responsive">
        <tr>
          <th>Transaction ID</th>
          <td id="txn_ref">SARTMADHDNSSDH</td>
        </tr>
        <tr>
          <th>MSISDN</th>
          <td id="customer">SARTMADHDNSSDH</td>
        </tr>
        <tr>
          <th>Amount</th>
          <td id="amount_result">SARTMADHDNSSDH</td>
        </tr>
        <tr>
          <th>Status</th>
          <td id="status">SARTMADHDNSSDH</td>
        </tr>
        <tr>
          <th>Error Message</th>
          <td id="error">No Error</td>
        </tr>
        <tr>
          <th>Date</th>
          <td id="date_now">SARTMADHDNSSDH</td>
        </tr>
        
      </table>
      <div class="form-group" >
            <button type="button" id="requery" class="btn btn-primary btn-block">Check Other</button>
        </div>
    </div>
    
  </div>

<script src="js/search.js"></script>
</body>
</html>