<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MTN Nigeria chargeback</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
     <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script> 
    <!-- <script src="https://cdn.jsdelivr.net/npm/vue@2"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
</head>
<body class="font-sans">
    <div id="app">
        <div class="">
            <div>
                <div class="rounded bg-gray-100 shadow-md text-gray-500 px-3 py-3">
                    <h1 class="uppercase text-sm">Chargeback Portal</h1>
                    <div id="formHandler" v-if="formShown">
                       <form @submit.prevent="processForm">
                            <input type="text" class="shadow-md w-96" name="txn_ref" v-model="txn_ref">
                            <br/>  <button class="bg-yellow-300" type="submit">Check</button>
                        </form> 
                    </div>
                    <div id="detail"  v-if="detailShown">
                        <p>{{ txn_ref }} </p>
                    </div>
                    
                </div>
                <p class="text-xs mt-8 text-gray-500 text-center 
                tracking-normal hover:underline">
                <i class="fa fa-lock"></i>
                Made by Codephree</p>
            </div>
            </div>
    </div>
    
    <script src="js/home.js"></script>
</body>
</html>